//
// Created by abijosh on 10/12/17.
//

#ifndef DGAME_WINDOWMANAGER_HPP
#define DGAME_WINDOWMANAGER_HPP

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Renderer.hpp"

class WindowManager {
public:
    /*
     * Sets values for the window to be created.
     * All values are mandatory as of now
     */
    WindowManager(unsigned int width, unsigned int height, const char* title);
    ~WindowManager();

    /*
     * should be manually called,
     * because it will help in future where we have to choose when to create the OpenGL context
     *
     * initializes glfw
     * creates window
     * sets glew parameter (glewExperimental = true)
     * creates OpenGL context
     * sets callback functions
     */
    void init();

    /*
     * initializes parameters for timer
     * basically sets prevTime to the current glfwTime - to compensate for the time spent for loading resources
     */
    void initTimer();

    /*
     * updates the window - planning on removing the same
     */
    void update(float delta);

    /*
     * check if window close is requested or not
     */
    bool closeNotRequested();

    /*
     * returns the calculated delta time
     */
    double getDeltaTime();

private:

    /*
     * Swap buffer is done here.
     * Apart from swapping if we plan on any post rendering effects then it has to be done here.
     * Example post rendering effect : 'Bloom effect'
     *
     * poll events is also done here where we request the OS to transfer all the input commands for this window
     */
    void postRender();

    /*
     * calculates deltaTime between previous and the current call to this function
     */
    void calculateDeltaTime();

private:
    double prevTime, delta;
    unsigned int width, height;
    const char *title;

    GLFWwindow* windowPtr;

    Renderer* renderer;
};


#endif //DGAME_WINDOWMANAGER_HPP
