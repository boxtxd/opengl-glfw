//
// Created by abijosh on 10/12/17.
//

#ifndef DGAME_RENDERER_HPP
#define DGAME_RENDERER_HPP

#include <GLFW/glfw3.h>

/*
 * Handles all the rendering related things in the game
 * */
class Renderer {
public:
    /*
     * the constructor sets default values for params red green blue and alpha
     * setting rgba values is not mandatory while creating Renderer
     */

    Renderer(float red = 0.2941f, float green = 0.03921f, float blue = 0.1294f, float alpha = 1.0f);
    ~Renderer();

    /*
     * handles the entire render cycle
     */
    void render(float delta);

private:
    /*
     * The screen is set before rendering.
     * If any render state is gonna change, the change should happen here only
     */
    void preRender();

private:
    float red, green, blue, alpha;
};


#endif //DGAME_RENDERER_HPP
