#include <iostream>
#include "WindowManager.hpp"
#include "Consts.hpp"

WindowManager *windowManager;

/*
 * function used to free all the memory allocated by us using the new keyword
 * because while allocating memory using the new keyword we reserve memory over the heap
 *
 * thus we have to free the same using the delete keyword - literally every new should have a delete
 *
 * study the difference between heap and stack memory allocation in C++
 */
void cleanUp(){
    delete windowManager;
}

int main() {
    windowManager = new WindowManager(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE);
    windowManager->init();

    windowManager->initTimer();
    do{
        float delta = static_cast<float> (windowManager->getDeltaTime());

        windowManager->update(delta);
    }while (windowManager->closeNotRequested());

    cleanUp();
}