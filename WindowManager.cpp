//
// Created by abijosh on 10/12/17.
//

#include "WindowManager.hpp"
#include <assert.h>

WindowManager::WindowManager(unsigned int width, unsigned int height, const char *title)
    :width(width)
    ,height(height)
    ,title(title)
{
}

WindowManager::~WindowManager() {
    delete renderer;
    glfwTerminate();
}

void WindowManager::init() {
    glfwInit();

    windowPtr = glfwCreateWindow(width, height, title, nullptr, nullptr);

    glfwMakeContextCurrent(windowPtr);

    /*
     * this can be written as glewExperimental = true
     * this gives a warning because glewExperimental is of type GLboolean
     * thus we static cast true to GLboolean - to avoid a warning
     */
    glewExperimental = static_cast<GLboolean>(true);
    glewInit();

    glEnable(GL_DEPTH_TEST);

    renderer = new Renderer();
}

void WindowManager::initTimer() {
    assert(windowPtr != nullptr);
    prevTime = glfwGetTime();
    delta = 0.0;
}

void WindowManager::update(float delta) {

    renderer->render(delta);
    postRender();
}

bool WindowManager::closeNotRequested() {

    /*
     * this can be written as return glfwWindowShouldClose(windowPtr)
     * but that gives a warning because the return type of glfwWindowShouldClose is an int
     * thus we check if it is not zero or not
     */
    return glfwWindowShouldClose(windowPtr) == 0;
}

double WindowManager::getDeltaTime() {
    return delta;
}

// private functions

void WindowManager::postRender() {
    glfwSwapBuffers(windowPtr);
    glfwPollEvents();
}

void WindowManager::calculateDeltaTime() {
    double currTime = glfwGetTime();
    delta = currTime - prevTime;
    prevTime = currTime;
}
