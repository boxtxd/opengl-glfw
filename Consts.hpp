//
// Created by abijosh on 10/12/17.
//

#ifndef DGAME_CONSTS_HPP
#define DGAME_CONSTS_HPP

constexpr int WINDOW_WIDTH{ 1024 };
constexpr int WINDOW_HEIGHT{ 720 };
constexpr char* WINDOW_TITLE{ "OGL Demo" };

#endif //DGAME_CONSTS_HPP
